module.exports = {
  building: {
    TOWER: {
      motif: ' T ',
      width: 3,
      height: 3
    },
    HOUSE: {
      motif: ' H ',
      width: 2,
      height: 2
    },
    WONDER: {
      motif: ' ¤ ',
      width: 5,
      height: 5
    },
    MARKET: {
      motif: ' € ',
      width: 3,
      height: 5
    },
    FORGE: {
      motif: ' F ',
      width: 4,
      height: 3
    },
    BARRACK: {
      motif: ' O ',
      width: 5,
      height: 5
    },
    FARM: {
      motif: '▒▒▒',
      width: 4,
      height: 4
    },
		CHURCH: {
			motif: ' ┼ ',
			width: 5,
			height: 5
		},
		UNIVERSITY: {
			motif: ' ♦ ',
			width: 5,
			height: 4
		},
		STABLE: {
			motif: ' ß ',
			width: 4,
			height: 4
		},
		ARCHERY: {
			motif: ' → ',
			width: 4,
			height:  4
		},
    GATE_H: {
      model: ["[  ", "   ", "  ]"]
    },
    GATE_V: {
      model: ["MMM", "   ", "WWW"]
    },
    WINDMILL: {
      motif: ' W ',
      width: 4,
      height: 4
    },
    CASTLE: {
      motif: ' C ',
      width: 7,
      height: 5
    }
  },
  wall: {
    HORIZONTAL: {
      SMALL: {
        motif: '---'
      },
      LARGE: {
        motif: '==='
      }
    },
    VERTICAL: {
      SMALL: {
        motif: ' | '
      },
      LARGE: {
        motif: '|||'
      }
    }
  }
};
